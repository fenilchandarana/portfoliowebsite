(function ($) {

  "use strict";

    // PRE LOADER
    $(window).load(function(){
      $('.preloader').fadeOut(1000); // set duration in brackets    
    });


    // MENU
    $('.menu-burger').on('click', function() {
      $('.menu-bg, .menu-items, .menu-burger').toggleClass('fs');
      $('.menu-burger').text() == "☰" ? $('.menu-burger').text('✕') : $('.menu-burger').text('☰');
    });


    // ABOUT SLIDER
    $('body').vegas({
        slides: [
            { src: 'images/index/Slide1.jpg' },
            { src: 'images/index/Slide2.jpg' },
            { src: 'images/index/Slide3.jpg' },
            { src: 'images/index/Slide4.jpg' },
            { src: 'images/index/Slide6.jpg' },
            { src: 'images/index/Slide7.jpg' },
            { src: 'images/index/Slide8.jpg' },
            { src: 'images/index/Slide9.jpg' },
        ],
        timer: false,
        transition: [ 'zoomIn', ]
    });

})(jQuery);
